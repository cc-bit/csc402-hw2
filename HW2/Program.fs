﻿(*Christopher Chitwood
   CSC402
   HW 2
   9/8/20*)

module HW2

open System

// Problem 1
let min3 (a,b,c) =
    if (a < b) then
        if (a < c)then
            a
        else
            c
    else 
        if (b < c) then  // I realise now that I left off the second nested loop in HW1.
            b            // Whoops.
        else
            c
     ;;
// END of Problem 1

// Problem 2
type Dinosaur = {name: string; weight: float; height: float};;
let tyranno = {name = "Tyrannosaurus"; weight = 7.0; height = 3.66}
let brachio = {name = "Brachiosaurus"; weight = 35.0; height = 9.4}

let nameOfHeavier x y  =
    if (x.weight > y.weight)then
        x.name
    else
        y.name
    ;;
// END of Problem 2
    
// Problem 3
let threeDDist (x1: float,y1,z1) (x2,y2,z2) =
    let sqrDeltaX = (x1 - x2) * (x1 - x2)  //sqrDelta == squared delta.  I think it reads pretty well.
    let sqrDeltaY = (y1 - y2) * (y1 - y2)
    let sqrDeltaZ = (z1 - z2) * (z1 - z2)
    sqrt(sqrDeltaX + sqrDeltaY + sqrDeltaZ)
    ;;
// END of Problem 3
    
// Problem 4
type Shape = | Circle of float
             | Square of float
             | Triangle of float*float*float
             | Rectangle of float*float
             ;;
let isShape = function
    | Circle r -> r > 0.0
    | Square a -> a > 0.0
    | Triangle(a,b,c) ->
        a > 0.0 && b > 0.0 && c > 0.0
        && a < b + c && b < c + a && c < a + b
    | Rectangle(a,b) -> a > 0.0 && b > 0.0
    ;;
let area x =
    if not (isShape x)
    then failwith "not a legal shape"
    else match x with
         | Circle r -> System.Math.PI * r * r
         | Square a -> a * a
         | Triangle(a,b,c) ->
            let s = (a + b + c)/2.0
            sqrt(s * (s - a) * (s - b) * (s - c))
         | Rectangle(a,b) -> a * b
    ;;

let perimeter x =
    if not (isShape x)
    then failwith "not a legal shape"
    else match x with
         | Circle r -> 2.0 * System.Math.PI * r
         | Square a -> a * 4.0
         | Triangle (a,b,c) -> a + b + c
         | Rectangle (a,b) -> (a * 2.0) + (b * 2.0)
    ;;

let circ = Circle 2.1;;
let sq = Square 3.6;;
let tri = Triangle (5.0,12.0,13.0);;
let rect = Rectangle (3.0,4.0);;
// END of Problem 4


// Problem 5
// Trying to keep line count down.
let makeShape (s: string) (a: float) (b: float option) (c: float option) =
   let returnShape tempShape =
     if not (isShape tempShape) then None
       else Some tempShape
   if (s = "Rectangle") && b <> None && c = None then
     let tempRectangle = Rectangle (a,Option.get(b))
     returnShape tempRectangle
   elif (s = "Square") && b = None && c = None then
     let tempSquare = Square a
     returnShape tempSquare
   elif (s = "Circle") && b = None && c = None then
     let tempCircle = Circle a
     returnShape tempCircle
   elif (s = "Triangle") && b <> None && c <> None then
     let tempTriangle = Triangle (a,Option.get(b),Option.get(c))
     returnShape tempTriangle
   else None;;
// END of Problem 5

[<EntryPoint>]
let main argv =
    0 // return an integer exit code
